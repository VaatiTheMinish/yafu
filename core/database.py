# core/database.py
from contextlib import asynccontextmanager

import asyncpg

from core.config import Config

# Global connection pool
pool = None


@asynccontextmanager
async def db_conn():
    global pool
    if pool is None:
        pool = await asyncpg.create_pool(Config.DATABASE_URI)
    async with pool.acquire() as conn:
        yield conn
