from config import Config

import asyncio
import asyncpg

async def initialize():
    print("Initializing database")
    # Connect to PostgreSQL
    pg_conn = await asyncpg.connect(Config.DATABASE_URI)

    # Create tables if they do not exist
    # Create tables
    await pg_conn.execute("""
        CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    """)

    await pg_conn.execute("""
        CREATE TABLE IF NOT EXISTS users (
            id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
            createdat TIMESTAMP DEFAULT NOW(),
            username VARCHAR(50) NOT NULL,
            password VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL,
            profilepic VARCHAR(255),
            api_key UUID,
            permissions jsonb,
            reset_code VARCHAR(255),
            reset_code_exp TIMESTAMP
        );
    """)

    await pg_conn.execute("""
        CREATE TABLE IF NOT EXISTS uploaded_files (
            id UUID DEFAULT uuid_generate_v4(),
            image_uuid UUID NOT NULL,
            image_upload_path VARCHAR(255) NOT NULL,
            short_url VARCHAR(6) NOT NULL,
            view_counter INTEGER DEFAULT 0,
            uploaded_at TIMESTAMP DEFAULT NOW(),
            uploaded_by UUID NOT NULL,
            FOREIGN KEY (uploaded_by) REFERENCES users(id),
            hash TEXT,
            filesize INTEGER,
            filetype TEXT
        );
    """)

    await pg_conn.execute("""
        CREATE TABLE IF NOT EXISTS short_urls (
            id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
            short_code VARCHAR(6) NOT NULL,
            original_url TEXT NOT NULL,
            hash TEXT,
            created_at TIMESTAMP DEFAULT NOW(),
            uploaded_by UUID NOT NULL,
            view_counter INTEGER DEFAULT 0,
            FOREIGN KEY (uploaded_by) REFERENCES users(id)
        );
    """)
    # Close the connection
    await pg_conn.close()
    print("Done")

asyncio.run(initialize())
