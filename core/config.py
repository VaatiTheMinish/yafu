from decouple import config


class Config:
    SECRET_KEY = config("SECRET_KEY", default="super_secret_key")

    DATABASE_URI = config(
        "DATABASE_URI", default="postgresql://username:password@host/pyuploader"
    )
    UPLOAD_DIR = config("UPLOAD_DIR", cast=str, default="./uploads")

    APP_NAME = config("APP_NAME", default="Image Uploader", cast=str)
    APP_HOST = config("APP_host", default="0.0.0.0", cast=str )
    APP_PORT = config("APP_PORT", default=5002, cast=int)
    APP_DEBUG = config("APP_DEBUG", default=False, cast=bool)
    REVERSE_PROXY_DOMAIN = config('REVERSE_PROXY_DOMAIN', default=APP_HOST ,cast=str)

    EMAIL_ENABLED = config("EMAIL_ENABLED", default=False, cast=bool)
    EMAIL_HOST = config("EMAIL_HOST", default=None, cast=str)
    EMAIL_PORT = config("EMAIL_PORT", default=0, cast=int)
    EMAIL_USER = config("EMAIL_USER", default=None, cast=str)
    EMAIL_PASSWORD = config("EMAIL_PASSWORD", default=None, cast=str)

    REQUIRED_SYM = config("REQUIRED_SYM", default=1, cast=int)
    REQUIRED_NUM = config("REQUIRED_NUM", default=1, cast=int)
    REQUIRED_LET  = config("REQUIRED_LET", default=1, cast=int)
    PASSWORD_MIN_LEN = config("PASSWORD_MIN_LEN", default=8, cast=int)

    registerratelimit = config("registerratelimit", default=1, cast=int)
    registerenabled = config("REGISTER_ENABLED", default=True, cast=bool)
    registerattempts = config("registerattempts", default=3, cast=int)
