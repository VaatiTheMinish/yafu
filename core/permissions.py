class Permissions:
    ADMIN = 3
    MODERATOR = 2
    USER = 1

    @staticmethod
    def has_permission(user_permissions_level, required_permissions_level):
        """
        Check if the user's permissions level is equal to or higher than the required permissions level.
        """
        return user_permissions_level >= required_permissions_level

    @staticmethod
    def is_admin(user_permissions_level):
        """
        Check if the user has admin permissions.
        """
        return user_permissions_level == Permissions.ADMIN

    @staticmethod
    def is_moderator(user_permissions_level):
        """
        Check if the user has moderator permissions.
        """
        return user_permissions_level == Permissions.MODERATOR

    @staticmethod
    def is_user(user_permissions_level):
        """
        Check if the user has user permissions.
        """
        return user_permissions_level == Permissions.USER