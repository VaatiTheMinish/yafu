# core/helpers.py
from core.database import db_conn
from core.config import Config
from aiosmtplib import SMTP

import os 
import aiofiles
import json

async def send_email(data):
    if Config.EMAIL_ENABLED != True:
    #skip sending email if the email server isnt enabled
        return

    # Parse the JSON string into a dictionary
    data_dict = json.loads(data)
    
    # Extract email details
    to = data_dict.get('to')
    subject = data_dict.get('subject')
    message = data_dict.get('message')
    
    # SMTP server details
    smtp_server = Config.EMAIL_HOST
    smtp_port = Config.EMAIL_PORT
    smtp_username = Config.EMAIL_USER
    smtp_password = Config.EMAIL_PASSWORD
    
    # Create an SMTP connection
    smtp = SMTP(hostname=smtp_server, port=smtp_port, use_tls=True)
    
    # Connect to the SMTP server
    await smtp.connect()
    
    # Login to the SMTP server
    await smtp.login(smtp_username, smtp_password)

    headers = {
        "Content-Type": "text/html",
        "Subject": subject,
        "From": smtp_username # Set the "From" header to the smtp_username
    }

    # Convert the headers dictionary to a string
    headers_str = "\n".join(f"{k}: {v}" for k, v in headers.items())

    # Send the email
    # Corrected line: Pass the sender's email as the first positional argument
    await smtp.sendmail(smtp_username, to, f"{headers_str}\n\n{message}")
    
    # Close the SMTP connection
    await smtp.quit()

async def change_email(email, username):
    async with db_conn() as conn:
        await conn.execute('UPDATE users SET email = $1 WHERE username = $2', email, username)
    return "Email updated successfully"

async def change_password(old_password, new_password, username, pwd_context):
    async with db_conn() as conn:
        # Fetch the current user's hashed password
        user = await conn.fetchrow('SELECT password FROM users WHERE username = $1', username)
        if user:
            # Verify the old password against the stored hashed password
            if pwd_context.verify(old_password, user['password']):
                # If the old password is correct, update the password
                await conn.execute('UPDATE users SET password = $1 WHERE username = $2', pwd_context.hash(new_password), username)
                return "Password updated successfully"
            else:
                return "Old password is incorrect", 400
        else:
            return "User not found", 404


async def upload_profile_picture(file, username):
    # Ensure the file object is correctly handled
    if not file or not file.filename:
        return "No file provided", 400

    # Construct the path for the new profile picture
    profile_picture_path = os.path.join(os.getcwd(), 'uploads', f'{username}.png')
    
    # Check if the file already exists and delete it if it does
    if os.path.exists(profile_picture_path):
        os.remove(profile_picture_path)
        print(f"Old image deleted: {profile_picture_path}")
    
    # Save the new image
    print(f"Saving new image to {profile_picture_path}")
    await file.save(profile_picture_path)
    
    # Update the database with the new profile picture path
    async with db_conn() as conn:
        await conn.execute('UPDATE users SET profilepic = $1 WHERE username = $2', profile_picture_path, username)
    
    return "Profile picture updated successfully"


async def handle_image(upload, image_path):
    file_url = f'/uploads/{os.path.basename(image_path)}'
    return {'id': upload['id'], 'short_url': upload['short_url'], 'file_url': file_url, 'file_type': 'image'}

async def handle_text(upload, image_path):
    async with aiofiles.open(image_path, 'r') as file:
        file_content = await file.read()
    return {'id': upload['id'], 'short_url': upload['short_url'], 'file_content': file_content, 'file_type': 'text'}

async def handle_video_audio(upload, image_path, file_type):
    file_url = f'/uploads/{os.path.basename(image_path)}'
    return {'id': upload['id'], 'short_url': upload['short_url'], 'file_url': file_url, 'file_type': file_type}

async def handle_other(upload, image_path):
    # The function now accepts two arguments, but you can ignore the second one if it's not needed
    return {'id': upload['id'], 'short_url': upload['short_url'], 'file_type': upload['filetype']}



file_type_handlers = {
    'image': handle_image,
    'text': handle_text,
    'video': lambda upload, image_path: handle_video_audio(upload, image_path, 'video'),
    'audio': lambda upload, image_path: handle_video_audio(upload, image_path, 'audio'),
    'other': lambda upload, image_path: handle_other(upload, image_path) # Adjusted to use a lambda function
}
