FROM python:3.11.9-alpine3.19

RUN mkdir /app

COPY . /app

WORKDIR /app

RUN pip3 install poetry

RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

CMD ["poetry", "run", "python3", "main.py"]
