{pkgs ? import <nixpkgs> {}}:
let
  mainFile = "main.py";
  run = pkgs.writeScriptBin "run" ''
    ${pkgs.poetry}/bin/poetry run python3 ${mainFile}
  '';
in
pkgs.mkShell {
  nativeBuildInputs = with pkgs.pkgsBuildHost; [python312 poetry run];
    shellHook = ''
    ${pkgs.poetry}/bin/poetry install
    ${pkgs.poetry}/bin/poetry shell
  '';
}
