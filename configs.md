Default Config Examples

    settings.ini
    [settings]
    SECRET_KEY=app_secret_key
    DATABASE_URI=postgresql://username:password@host/pyuploader
    UPLOAD_DIR=./uploads
    APP_NAME=YAFU
    APP_HOST=0.0.0.0
    APP_PORT=5002
    APP_DEBUG=True
    #set this if behind a reverse proxy ex: http://example.com/
    REVERSE_PROXY_DOMAIN=
    
    REGISTER_ENABLED=True
    
    
    registerratelimit=10
    registerattempts=3
    
    
    REQUIRED_SYM=2
    REQUIRED_NUM=2
    REQUIRED_LET=2
    PASSWORD_MIN_LEN = 8
    
    
    EMAIL_ENABLED=False
    EMAIL_HOST=
    EMAIL_PORT=
    EMAIL_USER=
    EMAIL_PASSWORD=

and for oauth_config.json an example provider would be like

    {
    
	    "providers": [
    
		    {
    
			    "name": "discord",
    
			    "client_id": "clientid",
    
			    "client_secret": "clientsecret",
    
			    "authorize_url": "https://discord.com/api/oauth2/authorize",
    
			    "access_token_url": "https://discord.com/api/oauth2/token",
    
			    "api_base_url": "https://discord.com/api/users/@me",
    
   			    "scope": "identify+email",
    
   			    "enabled": true,
    
			    "button_style": "bg-blue-600 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200",
    
   			    "redirect_url": "https://discord.com/oauth2/authorize?client_id=..."
    
	    }
    
	  ]
    
    }

