from werkzeug.security import generate_password_hash
from quart import Blueprint, session, request, url_for, redirect

from core.database import db_conn
from core.config import Config

import json
import hashlib
import requests

blueprint = Blueprint('oauth', __name__)


# Load OAuth configuration from oauth_config.json
with open('oauth_config.json') as f:
    oauth_config = json.load(f)

# OAuth providers configuration
OAUTH_PROVIDERS = oauth_config.get('providers', [])

# OAuth login route
@blueprint.route('/login/<provider_name>')
async def login(provider_name):
    provider_config = next((p for p in OAUTH_PROVIDERS if p['name'] == provider_name), None)
    if provider_config:
        redirect_uri = url_for('authorize', provider_name=provider_name, _external=True)  # Fix here
        authorization_url = f"{provider_config['authorize_url']}?client_id={provider_config['client_id']}&redirect_uri={redirect_uri}&scope={provider_config['scope']}&response_type=code"
        return redirect(authorization_url)
    else:
        return "Provider not found", 404


default_permissions = {
    "quota": 0,
    "enabled": True,
    "apiaccess": False,
    "fileupload": False,
    "uploadperm": False,
    "permissions_level": 1
}


@blueprint.route('/authorize/<provider_name>')
async def authorize(provider_name):
    registerenabled = Config.registerenabled
    code = request.args.get('code')
    provider_config = next((p for p in OAUTH_PROVIDERS if p['name'] == provider_name), None)
    if provider_config:
        if code:
            token_url = provider_config['access_token_url']
            hardcoded_redirect_uri = f"https://img.vaati.net/authorize/{provider_name}"
            token_params = {
                'client_id': provider_config['client_id'],
                'client_secret': provider_config['client_secret'],
                'code': code,
                'redirect_uri': hardcoded_redirect_uri,
                'grant_type': 'authorization_code'
            }
            response = requests.post(token_url, data=token_params)
            if response.status_code == 200:
                access_token = response.json().get('access_token')
                user_info_url = "https://discord.com/api/users/@me"
                headers = {'Authorization': f'Bearer {access_token}'}
                user_info_response = requests.get(user_info_url, headers=headers)
                if user_info_response.status_code == 200:
                    user_info = user_info_response.json()
                    username = user_info['username']
                    email = user_info.get('email', '') # Assuming email is optional
                    # Generate a placeholder hashed password
                    hashed_password = generate_password_hash(hashlib.sha256(username.encode()).hexdigest())
                    # Discord does not provide a 'permissions_level' directly, so you might need to set this based on your application's logic
                    default_permissions_level = 1 # Example default value
                    uploadperm = False

                    # Check if username already exists
                    async with db_conn() as conn:
                        existing_user = await conn.fetchrow('SELECT id FROM users WHERE username = $1', username)
                        if existing_user:
                            user_id = existing_user['id']
                        else:
                            if registerenabled == True:
                                permissions_json = json.dumps(default_permissions)
                                await conn.execute('INSERT INTO users (username, password, email, permissions) VALUES ($1, $2, $3, $4)', username, hashed_password, email, permissions_json)
                            else:
                                return '<DOCTYPE html><html lang="en"><head><meta charset="UTF-8"></head><body style="background-color: #333; color: white;"><h1>Registration is disabled</h1><p><a href="/login" style="color: white;">Return to login</a></body></html>'

                    session['username'] = username
                    session['permissions_level'] = default_permissions['permissions_level']
                    return redirect(url_for('index.home'))
                else:
                    error_message = user_info_response.json().get('error_description', 'Failed to fetch user info')
                    return f'<DOCTYPE html><html lang="en"><head><meta charset="UTF-8"></head><body style="background-color: #333; color: white;"><h1>{error_message} from {provider_name}</h1><p><a href="/login" style="color: white;">Return to login</a></body></html>', 500
            else:
                error_message = response.json().get('error_description', 'Failed to obtain access token')
                return f'<DOCTYPE html><html lang="en"><head><meta charset="UTF-8"></head><body style="background-color: #333; color: white;"><h1>{error_message} from {provider_name}</h1><p><a href="/login" style="color: white;">Return to login</a></body></html>', 500
        else:
            return f'<DOCTYPE html><html lang="en"><head><meta charset="UTF-8"></head><body style="background-color: #333; color: white;"><h1>Authorization code not found</h1><p><a href="/login" style="color: white;">Return to login</a></body></html>', 400
    else:
        return f'<DOCTYPE html><html lang="en"><head><meta charset="UTF-8"></head><body style="background-color: #333; color: white;"><h1>Authorization code not found</h1><p><a href="/login" style="color: white;">Return to login</a></body></html>', 404