#pages/auth.py 
from quart import Blueprint, render_template, session, request, redirect, url_for, jsonify
from core.database import db_conn
from passlib.context import CryptContext
from core.permissions import Permissions
from core.config import Config
from core.helpers import send_email

import re
import time
import random
import string
import json
import datetime
#import hashlib

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
blueprint = Blueprint('auth', __name__)
reset_codes = {}

with open('oauth_config.json') as f:
    oauth = json.load(f)


@blueprint.route('/reset-password', methods=['GET', 'POST'])
async def reset_password():
    if request.method == 'GET':
        code = request.args.get('code')
        code_provided = code is not None

        if code_provided:
            async with db_conn() as conn:
                user = await conn.fetchrow('SELECT id, reset_code_exp FROM users WHERE reset_code = $1', code)
                code_exists = user is not None
                if user and user['reset_code_exp'] and user['reset_code_exp'] > datetime.datetime.now():
                    code_valid = True
                else:
                    code_valid = False
            
            return await render_template('reset_password.html', code=code, code_exists=code_exists, code_provided=code_provided, code_valid=code_valid)
        else:
            return jsonify({'error': 'Reset code is required'}), 400

    elif request.method == 'POST':
        form_data = await request.form
        if 'username_or_email' in form_data:  # Request to send reset password email
            username_or_email = form_data.get('username_or_email')

            async with db_conn() as conn:
                user = await conn.fetchrow('SELECT id, email FROM users WHERE username = $1 OR email = $1', username_or_email)
                if user is None:
                    return jsonify({'error': 'User not found'}), 404
                
                reset_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
                reset_code_exp = datetime.datetime.now() + datetime.timedelta(hours=2)  # Expiry after 2 hours
                await conn.execute('UPDATE users SET reset_code = $1, reset_code_exp = $2 WHERE id = $3', reset_code, reset_code_exp, user['id'])

                reset_password_link = f"{Config.REVERSE_PROXY_DOMAIN}reset-password?code={reset_code}"
                email_data = json.dumps({
                    "to": user['email'],
                    "subject": f"{Config.APP_NAME} | Password Reset",
                    "message": f'''
                    <html>
                    <head>
                        <style>
                            body {{
                                font-family: Arial, sans-serif;
                            }}
                            .message {{
                                margin-bottom: 20px;
                            }}
                            .link {{
                                color: #007bff;
                                text-decoration: none;
                            }}
                        </style>
                    </head>
                    <body>
                        <div class="message">
                            <p><strong>Hi {username_or_email},</strong></p>
                            <p>To reset your password, please click the following link:</p>
                            <p><a class="link" href="{reset_password_link}">{reset_password_link}</a></p>
                            <p>If the link doesn't work, you can use this URL: {reset_password_link}</p>
                            <p>This link will expire in 2 hours from {datetime.datetime.now()}<p>
                        </div>
                        <small>If you did not request this email, you can ignore it.</small>
                    </body>
                    </html>
                    '''
                })
                await send_email(email_data)

                return jsonify({'message': 'Password reset email sent'}), 200
        
        else:  # Submit new password
            code = form_data.get('code')
            new_password = form_data.get('password')
            confirm_password = form_data.get('confirm_password')

            if new_password != confirm_password:
                return jsonify({'error': 'New password and confirm password do not match'}), 400

            hashed_password = pwd_context.hash(new_password)

            async with db_conn() as conn:
                user = await conn.fetchrow('SELECT id FROM users WHERE reset_code = $1', code)
                if user is None:
                    return jsonify({'error': 'Invalid or expired reset code'}), 400

                await conn.execute('UPDATE users SET password = $1, reset_code = NULL, reset_code_exp = NULL WHERE id = $2', hashed_password, user['id'])

            return jsonify({'message': 'Password reset successfully, you may login now'}), 200


@blueprint.route('/login', methods=['GET'])
async def loginpage():
    # Load the JSON file
    with open('oauth_config.json') as f:
        oauth_data = json.load(f)
    
    # Create a new list to hold the modified providers
    modified_providers = []
    
    # Iterate over the providers in the JSON data
    for provider in oauth_data['providers']:
        # Create a new dictionary with only the desired keys
        modified_provider = {
            'redirect_url': provider['redirect_url'],
            'name' : provider['name'],
            'button_style': provider['button_style'],
            'enabled': provider['enabled'],
            'button_style': provider['button_style']
        }
        # Append the modified provider to the new list
        modified_providers.append(modified_provider)
    
    # Pass the modified list to the template
    return await render_template('login.html', appname=Config.APP_NAME, providers=modified_providers)


@blueprint.route('/login', methods=['POST'])
async def login():
    form_data = await request.form
    username = form_data.get('username')
    password = form_data.get('password')
    
    async with db_conn() as conn:
        user = await conn.fetchrow('SELECT id, password, permissions FROM users WHERE username = $1', username)
        if user is None:
            return jsonify({'error': 'User not found'}), 200
        
        hashed_password = user['password']
        if not pwd_context.verify(password, hashed_password):
            return jsonify({'error': 'Invalid password'}), 200
        
        # Parse the permissions JSON string into a Python dictionary
        user_perms = json.loads(user['permissions'])

        # Check if the user's account is enabled
        if not user_perms.get('enabled', True):
            # If the account is not enabled, return a JSON response indicating that the account is disabled
            return jsonify({'error': 'Account is disabled'}), 200
        
        # the permissions as a dictionary
        permissions_level = user_perms.get('permissions_level', None)
        uploadperm = user_perms.get('uploadperm', False)
        apiaccess = user_perms.get('apiaccess', False)
        fileupload = user_perms.get('fileupload', False)
        enabled = user_perms.get('enabled', False)
        quota = user_perms.get('quota', None)
        
        session['username'] = username
        session['permissions_level'] = permissions_level
        session['uploadperm'] = uploadperm
        session['apiaccess'] = apiaccess
        session['fileupload'] = fileupload
        session['enabled'] = enabled
        session['quota'] = quota
    
    # Return a JSON response with a redirect URL
    return jsonify({'redirect': url_for('index.home')}), 200




@blueprint.route('/logout')
async def logout():
    session.pop('username', None)
    return redirect(url_for('index.home'))

default_permissions = {
    "quota": 0,
    "enabled": True,
    "apiaccess": False,
    "fileupload": False,
    "uploadperm": False,
    "permissions_level": 1
}


@blueprint.route('/register', methods=['GET', 'POST'])
async def register():
    registerenabled = Config.registerenabled
    if request.method == 'POST':

        form_data = await request.form
        username = form_data.get('username')
        password = form_data.get('password')
        email = form_data.get('email')
        confirm_email = form_data.get('confirm_email')
        confirm_password = form_data.get('confirm_password')

        attempts = session.get('register_attempts', [])
        current_time = time.time()
        attempts = [attempt for attempt in attempts if current_time - attempt < Config.registerratelimit * 60]
        if len(attempts) >= Config.registerattempts:
            return jsonify({'error': f'You have exceeded the registration attempts limit. Please wait {rate_limit_minutes} minutes before trying again.'}), 200

        # Validate form data
        if not username or not password or not email or not confirm_email or not confirm_password:
            return jsonify({'error': 'All fields are required'}), 200
        if email != confirm_email:
            return jsonify({'error': 'Emails do not match'}), 200
        if password != confirm_password:
            return jsonify({'error': 'Passwords do not match'}), 200

        # Basic validation checks
        email_regex = r"[^@]+@[^@]+\.[^@]+"
        if not re.match(email_regex, email):
            return jsonify({'error': 'Invalid email format'}), 200
        if len(username) < 4:
            return jsonify({'error': 'Username must be at least 4 characters long'}), 200

        # Password validation
        letters = Config.REQUIRED_LET
        numbers = Config.REQUIRED_NUM
        symbols = Config.REQUIRED_SYM

        password_regex = r"^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d]*$"
        if len(password) < Config.PASSWORD_MIN_LEN:
            return jsonify({'error': f'Password must be at least {min_length} characters long'}), 200
        # Count the occurrences of each character class
        letter_count = len(re.findall(r'[a-zA-Z]', password))
        number_count = len(re.findall(r'\d', password))
        symbol_count = len(re.findall(r"[!@#$%^&*()_+\-=\[\]{};':\"\\|,.<>\/?]", password))

        # Check if the counts meet the minimum requirements
        if letter_count < letters or number_count < numbers or symbol_count < symbols:
            return jsonify({'error': f'Password must contain at least {letters} letters, {numbers} numbers, and {symbols} symbols'}), 200


        # Hash the password
        hashed_password = pwd_context.hash(password)

        async with db_conn() as conn:
            # Check if username already exists
            existing_user = await conn.fetchrow('SELECT id FROM users WHERE username = $1', username)
            if existing_user:
                return jsonify({'error': 'Username already exists'}), 200

            # Insert the new user into the users table with default permissions
            permissions_json = json.dumps(default_permissions)
            await conn.execute('INSERT INTO users (username, password, email, permissions) VALUES ($1, $2, $3, $4)', username, hashed_password, email, permissions_json)
            
            # Fetch the newly created user's ID
            user_id = await conn.fetchval('SELECT id FROM users WHERE username = $1', username)
            
            # Store the username and permissions_level in the session
            session['username'] = username
            session['permissions_level'] = default_permissions['permissions_level']
        
        return jsonify({'redirect': url_for('index.home')}), 200
    else:
        # Serve the registration template for GET requests
        return await render_template('register.html', registerenabled=registerenabled, appname=Config.APP_NAME)
