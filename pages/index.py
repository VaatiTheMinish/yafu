from quart import Blueprint, render_template, session
from core.database import db_conn
from core.config import Config
from core.helpers import file_type_handlers 

import os
import json

blueprint = Blueprint('index', __name__)

from quart import Blueprint, render_template, session, redirect, url_for
from core.database import db_conn
from core.config import Config
from core.helpers import file_type_handlers 

import os
import json

blueprint = Blueprint('index', __name__)

@blueprint.route('/')
async def home():
    if 'username' in session:
        username = session['username']
        async with db_conn() as conn:
            user_details = await conn.fetchrow('SELECT username, profilepic, id, permissions FROM users WHERE username = $1', username)
            if user_details is None:
                return "User details not found", 404

            # Parse the permissions JSON string into a Python dictionary
            user_perms = json.loads(user_details['permissions'])

            # Check if the user's account is enabled
            if not user_perms.get('enabled', False):
                # If the account is not enabled, redirect to /logout
                return redirect(url_for('auth.logout'))

            # Fetch all uploaded files for the user
            user_uploads = await conn.fetch('SELECT id, short_url, image_upload_path, filetype, filesize FROM uploaded_files WHERE uploaded_by = $1', user_details['id'])
            user_links = await conn.fetch('SELECT id, short_code, original_url, created_at, view_counter FROM short_urls WHERE uploaded_by = $1', user_details['id'])

            # Ensure user_uploads is a list, even if it's empty
            if user_uploads is None:
                user_uploads = []

            # Calculate total file size
            total_file_size = sum(upload['filesize'] for upload in user_uploads)

            # Prepare file URLs or content for each uploaded file
            prepared_uploads = []
            for upload in user_uploads:
                image_path = upload['image_upload_path']
                file_type = upload['filetype']
                if os.path.exists(image_path):
                    handler = file_type_handlers.get(file_type, file_type_handlers['other'])
                    prepared_upload = await handler(upload, image_path)
                    prepared_uploads.append(prepared_upload)

            if user_details['profilepic'] != None:
                profpic = user_details['profilepic'].replace(os.getcwd(), "")
            else:
                profpic = 0

            # Build the context dictionary
            context = {
                'name': user_details['username'],
                'profilepic': profpic,
                'uploadcount': len(user_uploads),
                'uploadperm': user_perms.get('uploadperm', False),
                'permissions_level': user_perms.get('permissions_level', None),
                'quota': user_perms.get('quota', None),
                'fileupload': user_perms.get('fileupload', False),
                'enabled': user_perms.get('enabled', False),
                'apiaccess': user_perms.get('apiaccess', False),
                'total_file_size': total_file_size,
                'user_uploads': prepared_uploads,
                'user_links': user_links
            }
            # Pass the context dictionary to the template using **context
            return await render_template('home.html', **context, appname=Config.APP_NAME)
    else:
        return await render_template('index.html', appname=Config.APP_NAME)
