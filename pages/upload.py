import hashlib
import os
import random
import string
import uuid
import json

from quart import Blueprint, abort, jsonify, request, session, redirect

from core.config import Config
from core.database import db_conn

blueprint = Blueprint("upload", __name__)

file_type_categories = {
    "image": [
        ".png",
        ".jpg",
        ".jpeg",
        ".gif",
        ".webp",
        ".bmp",
        ".svg",
        ".ico",
        ".tiff",
        ".heif",
    ],
    "text": [
        ".txt",
        ".py",
        ".lua",
        ".c",
        ".cpp",
        ".js",
        ".html",
        ".css",
        ".json",
        ".md",
        ".xml",
        ".csv",
        ".ini",
        ".yml",
        ".log",
    ],
    "video": [
        ".mp4",
        ".mkv",
        ".avi",
        ".mov",
        ".flv",
        ".wmv",
        ".m4v",
        ".3gp",
        ".mpg",
        ".mpeg",
    ],
    "audio": [
        ".mp3",
        ".wav",
        ".flac",
        ".m4a",
        ".aac",
        ".ogg",
        ".wma",
        ".m4b",
        ".aiff",
        ".midi",
    ],
    "executable": [".exe", ".bin", ".app", ".zip", ".jar"],  # doesnt work now
}


def generate_short_url():
    characters = string.ascii_letters + string.digits
    return "".join(random.choice(characters) for _ in range(6))


async def handle_other(upload, image_path):
    # The function now accepts two arguments, but you can ignore the second one if it's not needed
    return {
        "id": upload["id"],
        "short_url": upload["short_url"],
        "file_type": upload["filetype"],
    }


@blueprint.route("/delurl/<id>", methods=["DELETE"])
async def delete_url(id):
    # Check session and permissions
    if "username" not in session:
        return abort(403, description="Session not found or expired")

    # Get the current user's ID from the session
    async with db_conn() as conn:
        user_id = await conn.fetchval(
            "SELECT id FROM users WHERE username = $1", session.get("username", "")
        )

    # Verify if the user has upload permission
    if not session.get("uploadperm", False):
        return abort(403, description="Upload permission denied")

    # Check if the URL exists and was uploaded by the current user
    async with db_conn() as conn:
        result = await conn.fetchrow(
            "SELECT * FROM short_urls WHERE short_code = $1 AND uploaded_by = $2",
            id, user_id
        )
        if not result:
            return jsonify({"error": "URL not found or not authorized to delete"}), 404

    # Delete the URL
    async with db_conn() as conn:
        await conn.execute(
            "DELETE FROM short_urls WHERE short_code = $1", id
        )

    return jsonify({"message": "URL successfully deleted"}), 200


@blueprint.route("/shorten", methods=["POST",'GET'])
async def shorten_url():
        # Check session and permissions
    if "username" not in session:
        return abort(403, description="Session not found or expired")
    if not session.get("uploadperm", False):
        return abort(403, description="Upload permission denied")
    # Attempt to parse the request body as form data
    data = await request.form
    print(data)

    # Check if the request body is not None and contains the "url" key
    # Check if the "url" field exists in the form data
    if "url" not in data:
        return jsonify({"error": "URL is required"}), 400

    url = data["url"]


    # Generate a unique short code for the URL
    short_code = generate_short_url()

    # Hash the URL to ensure uniqueness and security
    url_hash = hashlib.sha256(url.encode()).hexdigest()

    # Get the current user's ID from the session
    async with db_conn() as conn:
        user_id = await conn.fetchval(
            "SELECT id FROM users WHERE username = $1", session.get("username", "")
        )

    async with db_conn() as conn:
        await conn.execute(
            """
            INSERT INTO short_urls (short_code, original_url, hash, uploaded_by)
            VALUES ($1, $2, $3, $4)
            """,
            short_code,
            url,
            url_hash,
            user_id,
        )

    # Return the short URL to the client
    return jsonify({"short_url": f"/{short_code}"}), 200


@blueprint.route('/u/<short_url>')
async def url_redir(short_url):
    async with db_conn() as conn:  # Acquire a new connection from the pool
        # Query the database to retrieve the original URL associated with the short URL
        original_url = await conn.fetchval(
            "SELECT original_url FROM short_urls WHERE short_code = $1", short_url
        )

        # If the original URL is found, increment the view counter and redirect the user
        if original_url:
            # Increment the view counter in the database
            await conn.execute(
                "UPDATE short_urls SET view_counter = view_counter + 1 WHERE short_code = $1", short_url
            )
            # Redirect the user to the original URL
            return redirect(original_url)
        else:
            # If the original URL is not found, return an appropriate response (e.g., 404 Not Found)
            return "URL not found", 404


@blueprint.route("/upload", methods=["POST"])
async def upload_file():
    # Check session and permissions
    if "username" not in session:
        return abort(403, description="Session not found or expired")
    if not session.get("uploadperm", False):
        return abort(403, description="Upload permission denied")

    # Check for API key in headers
    api_key = request.headers.get("X-API-Key")
    if api_key:
        async with db_conn() as conn:
            user = await conn.fetchrow(
                "SELECT * FROM users WHERE api_key = $1", api_key
            )
            if not user:
                return abort(401, description="Invalid API key")

    files = await request.files
    if "file" not in files or not files["file"].filename:
        return "No file part", 400

    file = files["file"]
    file_hash = hashlib.sha256(file.read()).hexdigest()
    file.seek(0) # Reset the file stream position to the beginning

    # Calculate the file size
    file_size = len(file.read())
    file.seek(0) # Reset the file stream position to the beginning again

    # Determine the file type based on the original file extension
    file_extension = os.path.splitext(file.filename)[1].lower()
    file_type = "unknown"
    for category, extensions in file_type_categories.items():
        if file_extension in extensions:
            file_type = category
            break

    if file_type == "unknown":
        return (
            jsonify({"message": "File type not allowed", "error": "Invalid file type"}),
            400,
        )

    # Check if the file hash already exists in the database
    async with db_conn() as conn:
        existing_file = await conn.fetchval(
            "SELECT * FROM uploaded_files WHERE hash = $1", file_hash
        )
        if existing_file:
            return (
                jsonify({"message": "File is a duplicate", "error": "Duplicate file"}),
                400,
            )

        # Fetch the user's permissions from the database
        user_permissions = await conn.fetchval(
            "SELECT permissions FROM users WHERE username = $1", session["username"]
        )
        if user_permissions is None:
            return "User permissions not found", 404

        # Parse the permissions JSON string into a Python dictionary
        user_permissions_dict = json.loads(user_permissions)
        user_quota = user_permissions_dict.get('quota', None)

        # Check if the user has reached their quota
        total_file_size = await conn.fetchval(
            "SELECT SUM(filesize) FROM uploaded_files WHERE uploaded_by = (SELECT id FROM users WHERE username = $1)",
            session["username"],
        )
        if total_file_size is None:
            total_file_size = 0

        if user_quota <= -1:
            pass
        else:
            if total_file_size + file_size > user_quota:
                return (
                    jsonify(
                        {
                            "message": "You have reached your storage limit. Unable to upload more files.",
                            "error": "Quota exceeded",
                        }
                    ),
                    400,
                )

        # Prepare for file upload
        image_uuid = uuid.uuid4()
        short_url = generate_short_url()
        upload_dir = str(Config.UPLOAD_DIR)
        if not os.path.exists(upload_dir):
            os.makedirs(upload_dir)
        file_path = os.path.join(upload_dir, f"{image_uuid}{file_extension}")
        await file.save(file_path)

        # Fetch the user's ID from the database using their username
        user_id = await conn.fetchval(
            "SELECT id FROM users WHERE username = $1", session["username"]
        )
        if user_id is None:
            return "User not found", 404

        # Insert the new file record into the database
        await conn.execute(
            """
            INSERT INTO uploaded_files (image_uuid, image_upload_path, short_url, uploaded_by, hash, filetype, filesize)
            VALUES ($1, $2, $3, $4, $5, $6, $7)
        """,
            image_uuid,
            file_path,
            short_url,
            user_id,
            file_hash,
            file_type,
            file_size,
        )

    return (
        jsonify({"message": "File uploaded successfully", "short_url": short_url}),
        200,
    )
