import json
import shutil

# used_media_space_gb = sum(
from quart import (Blueprint, jsonify, redirect, render_template, request, session, url_for)

from core.config import Config
from core.database import db_conn
from core.helpers import change_email, change_password, upload_profile_picture
from core.permissions import Permissions

import os
blueprint = Blueprint("admin", __name__)


allowed_actions = {
    "apiaccess": bool,
    "enabled": bool,
    "uploadperm": bool,
    "fileupload": bool,
    "quota": int,
    "delete": bool,  # New action for deleting a user
}

registration_status = True


@blueprint.route("/set_registration_status", methods=["POST"])
async def set_registration_status():
    global registration_status
    data = await request.json
    registration_status = data["status"]
    return jsonify({"message": "Registration status updated successfully"})


@blueprint.route("/get_registration_status", methods=["GET"])
async def get_registration_status():
    global registration_status
    return jsonify({"status": registration_status})


@blueprint.route("/updateuser/<uuid:uuid>", methods=["POST"])
async def updateuser(uuid):
    if "permissions_level" not in session or not Permissions.is_admin(
        session["permissions_level"]
    ):
        referrer = request.referrer or url_for("index.home")
        return redirect(referrer)

    data = await request.get_json()
    action = data.get("action")
    value = data.get("value")
    print(f"post got: {action} - {value}")

    if action not in allowed_actions or (
        action != "delete" and not isinstance(value, allowed_actions[action])
    ):
        error_message = (
            f"Invalid action: {action}"
            if action not in allowed_actions
            else f"Invalid data type for action {action}: expected {allowed_actions[action].__name__}, got {type(value).__name__}"
        )
        return jsonify({"error": error_message}), 400

    if action == "delete":
        # Handle the delete action
        async with db_conn() as conn:
            # Delete uploaded files
            delete_uploaded_files_sql = (
                "DELETE FROM uploaded_files WHERE uploaded_by = $1"
            )
            await conn.execute(delete_uploaded_files_sql, uuid)

            # Delete user
            delete_user_sql = "DELETE FROM users WHERE id = $1"
            await conn.execute(delete_user_sql, uuid)

            return (
                jsonify(
                    {
                        "message": f"User with UUID {uuid} and all related data deleted successfully"
                    }
                ),
                200,
            )
    else:
        # Fetch the current permissions
        async with db_conn() as conn:
            fetch_permissions_sql = "SELECT permissions FROM users WHERE id = $1"
            permissions_json = await conn.fetchval(fetch_permissions_sql, uuid)
            permissions = json.loads(permissions_json)

            # Update the specific key in the permissions dictionary
            key_to_update = action.replace("_", "")
            permissions[key_to_update] = value

            # Update the permissions column in the database with the new JSONB string
            update_permissions_sql = "UPDATE users SET permissions = $1 WHERE id = $2"
            await conn.execute(update_permissions_sql, json.dumps(permissions), uuid)

            return (
                jsonify({"message": f"Permission {action} updated successfully"}),
                200,
            )



@blueprint.route("/admin")
async def admin_dash():
    if "username" not in session or not Permissions.is_admin(
        session["permissions_level"]
    ):
        referrer = request.referrer or url_for(
            "index.home"
        )  # Fallback to home page if referrer is not available
        return redirect(referrer)
    # Get disk usage for the 'uploads/' directory
    total, used, free = shutil.disk_usage(
        str(Config.UPLOAD_DIR)
    )  # Replace '/path/to/uploads' with the actual path
    total_space_gb = total / (1024.0**3)  # Convert bytes to gigabytes
    used_space_gb = used / (1024.0**3)  # Convert bytes to gigabytes
    free_space_gb = free / (1024.0**3)  # Convert bytes to gigabytes

    # Calculate the percentage of used space
    used_percentage = (used_space_gb / total_space_gb) * 100

    async with db_conn() as conn:
        users = await conn.fetch("SELECT username, profilepic, email, id FROM users")
        user_uploads = await conn.fetch(
            "SELECT uploaded_by, COUNT(*) as file_count FROM uploaded_files GROUP BY uploaded_by"
        )
        user_file_sizes = await conn.fetch(
            "SELECT uploaded_by, SUM(filesize) as total_file_size FROM uploaded_files GROUP BY uploaded_by"
        )

    total_users = len(users)
    total_files = sum(record["file_count"] for record in user_uploads)
    # used_media_space_gb = sum(
    # record["total_file_size"] for record in user_file_sizes
    # ) (
    # 1024.0**3
    # )  # Convert bytes to gigabytes

    context = {
        "total_space": total_space_gb,
        "used_space": used_space_gb,
        "free_space": free_space_gb,
        "used_percentage": used_percentage,
        "total_users": total_users,
        "total_files": total_files,
        "used_media_space_gb": 0,
    }

    # Pass the context dictionary to the template
    return await render_template("admin_dash.html", **context)


@blueprint.route("/admin/users")
async def admin():
    if "username" not in session or not Permissions.is_admin(
        session["permissions_level"]
    ):
        referrer = request.referrer or url_for("index.home")
        return redirect(referrer)

    async with db_conn() as conn:
        # Fetch users along with their permissions
        users = await conn.fetch("SELECT username, profilepic, email, id, permissions FROM users")

        # Fetch user uploads and file sizes
        user_uploads = await conn.fetch(
            "SELECT uploaded_by, COUNT(*) as file_count FROM uploaded_files GROUP BY uploaded_by"
        )
        user_file_sizes = await conn.fetch(
            "SELECT uploaded_by, SUM(filesize) as total_file_size FROM uploaded_files GROUP BY uploaded_by"
        )

        # Convert user_uploads and user_file_sizes to dictionaries for easy lookup
        uploads_dict = {
            upload["uploaded_by"]: upload["file_count"] for upload in user_uploads
        }
        file_sizes_dict = {
            upload["uploaded_by"]: upload["total_file_size"]
            for upload in user_file_sizes
        }

        # Convert users to a list of dictionaries and add the file count, permissions, and total file size to each user
        users = [dict(user) for user in users] # Convert each user to a dictionary
        for user in users:
            user["file_count"] = uploads_dict.get(user["id"], 0)
            user["total_file_size"] = file_sizes_dict.get(user["id"], 0)
            # Parse the permissions JSON string into a Python dictionary
            user["permissions"] = json.loads(user["permissions"])
            # Process the profile picture path if it exists
            if user["profilepic"]:
                relative_path = os.path.relpath(user["profilepic"], os.getcwd())
                user["profilepic"] = "/uploads/" + relative_path.lstrip("/")
            else:
                user["profilepic"] = "default_image_url_here" # Or any other default value

        # Send the users list with their permissions and total file size to the template
        return await render_template("admin_users.html", users=users, appname=Config.APP_NAME)