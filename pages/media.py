from quart import session, Blueprint, jsonify, send_from_directory, render_template, abort,send_file
from core.database import db_conn
from core.config import Config
from core.helpers import file_type_handlers 


import os 
import mimetypes

blueprint = Blueprint('media', __name__)


file_type_categories = {
    'image': ['.png', '.jpg', '.jpeg', '.gif', '.webp', '.bmp', '.svg', '.ico', '.tiff', '.heif'],
    'text': ['.txt', '.py', '.lua', '.c', '.cpp', '.js', '.html', '.css', '.json', '.md', '.xml', '.csv', '.ini', '.yml', '.log'],
    'video': ['.mp4', '.mkv', '.avi', '.mov', '.flv', '.wmv', '.m4v', '.3gp', '.mpg', '.mpeg'],
    'audio': ['.mp3', '.wav', '.flac', '.m4a', '.aac', '.ogg', '.wma', '.m4b', '.aiff', '.midi'],
    'other': ['.exe', '.bin', '.app', '.zip', '.jar']
}


@blueprint.route('/download/<short_url>')
async def download_file(short_url):
    async with db_conn() as conn:
        image_record = await conn.fetchrow('SELECT * FROM uploaded_files WHERE short_url = $1', short_url)
        if image_record:
            image_path = image_record['image_upload_path']
            if os.path.exists(image_path):
                # Determine the MIME type based on the file extension
                mime_type, _ = mimetypes.guess_type(image_path)
                response = await send_file(image_path, mimetype=mime_type, as_attachment=True)
                return response
            else:
                abort(404, description="File not found")
        else:
            abort(404, description="File not found")


@blueprint.route('/delete/<uuid:uuid>', methods=['GET','POST'])
async def delete_item(uuid):
    # Assuming 'username' is stored in the session
    username = session.get('username')
    if not username:
        return jsonify({"message": "false"}), 401

    async with db_conn() as conn:
        # Fetch the user ID associated with the username
        user_data = await conn.fetchrow('SELECT id FROM users WHERE username = $1', username)
        if not user_data:
            return jsonify({"message": "false"}), 401

        user_id = user_data['id']

        # Fetch the file data using the provided UUID
        file_data = await conn.fetchrow('SELECT * FROM uploaded_files WHERE id = $1', uuid)
        if not file_data:
            return jsonify({"message": "false"}), 404

        # Assuming the user ID is stored in the file data under a key named 'uploaded_by'
        if file_data['uploaded_by'] == user_id:
            # Delete the file record from the database
            await conn.execute('DELETE FROM uploaded_files WHERE id = $1', uuid)
            # Optionally, delete the file from the filesystem if it exists
            # This part depends on your application's setup
            # For example:
            if os.path.exists(file_data['image_upload_path']):
                os.remove(file_data['image_upload_path'])
            return jsonify({"message": "file deleted"}), 200
        else:
            return jsonify({"message": "Unauthorized"}), 403


@blueprint.route('/i/<short_url>')
async def serve_image(short_url):
    # Set admin to True if there is an active session, otherwise False
    admin = 'username' in session

    async with db_conn() as conn:
        image_record = await conn.fetchrow('SELECT * FROM uploaded_files WHERE short_url = $1', short_url)
        if image_record:
            image_path = image_record['image_upload_path']
            image_path = image_record['image_upload_path']
            file_type = image_record['filetype']
            if os.path.exists(image_path):
                file_path = image_path.replace('./', '')
                handler = file_type_handlers.get(file_type, file_type_handlers['other'])
                prepared_upload = await handler(image_record, image_path)
                return await render_template('display_file.html', **prepared_upload, file_path=file_path, admin=admin, appname=Config.APP_NAME)
            else:
                abort(404, description="File not found")
        else:
            return await send_file('uploads/404.png', mimetype='image/png')


@blueprint.route('/uploads/<path:filename>')
async def serve_uploaded_file(filename):
    uploads_dir = os.path.join("/mnt/user/data/CodeShit/python/yafu", 'uploads')
    file_path = os.path.join(uploads_dir, filename)
    
    if not os.path.exists(file_path):
        abort(404, description="File not found")
    
    # Determine the MIME type based on the file extension
    mime_type, _ = mimetypes.guess_type(file_path)
    
    # Serve the file with the correct MIME type
    return await send_from_directory(uploads_dir, filename, mimetype=mime_type)

