from quart import session, Blueprint, request, jsonify, redirect, url_for, render_template
from core.helpers import change_email, change_password, upload_profile_picture, send_email
from core.config import Config
from core.database import db_conn

import json

blueprint = Blueprint('settings', __name__)

@blueprint.route('/settings', methods=['GET'])
async def settingspage():
    # Example of setting a value in the session
    print(session['username'])
        # Fetch the user's permissions from the database
    async with db_conn() as conn:
        user_perms = await conn.fetchval('SELECT permissions FROM users WHERE username = $1', session['username'])
        # Convert the JSONB permissions into a Python dictionary
        permissions = json.loads(user_perms) if user_perms else {}
    return await render_template('settings.html', appname=Config.APP_NAME, permissions=permissions)


#to do: make this not be if statements
@blueprint.route('/settings', methods=['POST'])
async def settings():
    form_data = await request.form
    action = form_data.get('action')
    username = session.get('username')

    if action == 'change_email':
        email = form_data.get('email')
        if email:
            await change_email(email, username)
            return redirect(url_for('index.home'))
        else:
            return "Email not provided", 400
    elif action == 'change_password':
        old_password = form_data.get('old_password') # Ensure this matches the name attribute in your form
        new_password = form_data.get('password') # Ensure this matches the name attribute in your form
        if old_password and new_password: # Check if both passwords are provided
            result = await change_password(old_password, new_password, username, pwd_context)
            if isinstance(result, str) and "updated successfully" in result:
                return redirect(url_for('index.home'))
            else:
                return result
        else:
            return "Old password and/or new password not provided", 400
    elif action == 'upload_profile_picture':
        # Correctly await the request.files coroutine
        files = await request.files
        if 'profile_picture' in files:
            file = files['profile_picture']
            if file.filename != '':
                result = await upload_profile_picture(file, username)
                if isinstance(result, str) and "updated successfully" in result:
                    return redirect(url_for('index.home'))
                else:
                    return result
            else:
                return "No file selected", 400
        else:
            return "No file part", 400
    else:
        return "Invalid action", 400

