import importlib
from pathlib import Path
from quart import (Quart, abort, render_template, g)

from core.config import Config


app = Quart(__name__)
app.secret_key = Config.SECRET_KEY


@app.template_filter("bytes_to_human")
def bytes_to_human(value):
    """
    Convert bytes to a human-readable format (MB or GB).
    """
    if value < 0:
        return 'Unl'

    units = ["B", "KB", "MB", "GB"]
    value = float(value)
    for unit in units:
        if value < 1024.0:
            return f"{value:.2f} {unit}"
        value /= 1024.0

    return f"{value:.2f} TB"



# -----------------------------
@app.before_request
async def capture_exception():
    try:
        # This block is intentionally left empty. 
        # The middleware will capture any exceptions that occur during request handling.
        pass
    except Exception as e:
        # Store the exception in the request context
        g.exception = e
        # Re-raise the exception to trigger the error handler
        raise


@app.errorhandler(400)
async def bad_request(error):
    return (
        await render_template(
            "error.html",
            error_code=400,
            error_message="Bad Request",
            generic_message="Your browser sent a request that this server could not understand.",
        ),
        400,
    )


@app.errorhandler(401)
async def unauthorized(error):
    return (
        await render_template(
            "error.html",
            error_code=401,
            error_message="Unauthorized",
            generic_message="You are not authorized to access this resource. Please ensure you have the correct permissions or credentials.",
        ),
        401,
    )


@app.errorhandler(403)
async def forbidden(error):
    return (
        await render_template(
            "error.html",
            error_code=403,
            error_message="Forbidden",
            generic_message="You do not have permission to view this page.",
        ),
        403,
    )


@app.errorhandler(404)
async def page_not_found(error):
    return (
        await render_template(
            "error.html",
            error_code=404,
            error_message="Page Not Found",
            generic_message="The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.",
        ),
        404,
    )



@app.errorhandler(405)
async def method_not_allowed(error):
    return (
        await render_template(
            "error.html",
            error_code=405,
            error_message="Method Not Allowed",
            generic_message="The method specified in the request is not allowed for the resource identified by the request URI.",
        ),
        405,
    )


if not Config.APP_DEBUG:

    @app.errorhandler(Exception)
    async def handle_exception(error):
        # Log the error for debugging purposes
        app.logger.error(f"Internal Server Error: {str(error)}")

        # Pass the error message to the template
        return (
            await render_template(
                "error.html",
                error_code=500,
                error_message="Internal Server Error",
                generic_message=str(error),
            ),
            500,
        )


@app.after_request
async def prevent_caching(response):
    # Set headers to prevent caching
    response.headers["Cache-Control"] = "no-store, no-cache, must-revalidate, max-age=0"
    response.headers["Pragma"] = "no-cache"
    return response


@app.route("/error/<int:code>")
async def trigger_error(code):
    if code in [400, 401, 403, 404, 405, 500]:
        abort(code)
    else:
        abort(400, description="Invalid error code")


# Define the directories to search for route files
directories_to_search = ["api", "pages"]

# Iterate over each directory to find and register blueprints
for directory in directories_to_search:
    routes_dir = Path(__file__).parent / directory
    for route_file in routes_dir.glob("*.py"):
        if route_file.stem != "__init__":  # Skip the __init__.py file
            module_name = f"{directory}.{route_file.stem}"
            module = importlib.import_module(module_name)
            if hasattr(module, "blueprint"):
                print(f"Registering blueprint from {module_name}")
                app.register_blueprint(module.blueprint)


if __name__ == "__main__":
    app.run(host=Config.APP_HOST, port=Config.APP_PORT, debug=Config.APP_DEBUG)
